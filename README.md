TestTask
-----

Install
-----
**CMD command: composer install**<br />
To create autoload.php
 
**Create database 'port_content' and import db_dump.sql**<br />
Or rewrite the database name in _config/parameters.json_ file. 

**Document root: /web/**<br />
The routing module needs the .htaccess config.<br />
I used xampp for development... i'm not sure, how a Nginx server should be configured to do the same.

**Curl** is needed for connection to port.hu. 

I think it should work with PHP 5.6, although it wasn't tested.


Urls
-----
- http://portcontent/load/select-channels/
- http://portcontent/channel-content


Files
-----
- src/Port/Controller/
- src/Port/Service/
- src/Port/view/


Comment
-----
This Egf abomination is my creation.<br />
It's just experimental, but it can be useful when i cannot use Symfony.