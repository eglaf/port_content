<?php

namespace Egf\Service\MyDb\Helper\DbWhere;

/**
 * Class Boolean
 */
class Boolean extends Base {

    /** @var string Type. */
    protected $sType = 'i';

	/**
	 * A basic condition equation string.
	 * @return string
	 */
	public function getConditionEquation() {
		return ' = ';
	}

}