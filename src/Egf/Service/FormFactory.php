<?php

namespace Egf\Service;

use \Egf\Ancient;

/**
 * Class FormFactory
 */
class FormFactory extends Ancient\Service {
	
	/**
	 * It creates a new form class and add the App object.
	 * @param string $formClass
	 * @return mixed
	 */
	public function newForm($formClass) {
		return (new $formClass($this->app));
	}
	
}