<?php

namespace Egf\Service;

use \Egf\Ancient;

/**
 * Class Request
 *
 * todo Some security...
 */
class Request extends Ancient\Service {
	
	/**
	 * Get query data.
	 * @param string $key
	 * @param null   $default
	 * @return mixed|null
	 */
	public function getQuery($key, $default = NULL) {
		if (isset($_GET[$key])) {
			return $_GET[$key];
		}
		
		return $default;
	}
	
	/**
	 * Get request data.
	 * @param string $key
	 * @param null   $default
	 * @return mixed|null
	 */
	public function getRequest($key, $default = NULL) {
		if (isset($_POST[$key])) {
			return $_POST[$key];
		}
		
		return $default;
	}
	
	/**
	 * Get file from request.
	 * @param string $key
	 * @param null   $default
	 * @return mixed|null
	 */
	public function getFile($key, $default = NULL) {
		if (isset($_FILES[$key])) {
			return $_FILES[$key];
		}
		
		return $default;
	}
	
}