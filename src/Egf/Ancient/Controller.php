<?php

namespace Egf\Ancient;

use \Egf\App;

/**
 * Class Controller
 */
abstract class Controller {
	
	/** @var App */
	protected $app;
	
	
	/**
	 * Controller constructor.
	 * @param App $app
	 */
	public function __construct(App $app) {
		$this->app = $app;
		
		$this->init();
	}
	
	/**
	 * Does stuff in construct.
	 */
	public function init() {
	}
	
	
	/**
	 * Gets a service.
	 * @param string $sService
	 * @return object
	 */
	protected function getService($sService) {
		return $this->app->getService($sService);
	}
	
	/**
	 * Gets a config value.
	 * @param string $sKey
	 * @param mixed  $xDefault
	 * @return mixed
	 */
	protected function getConfig($sKey, $xDefault = NULL) {
		return $this->app->getConfig($sKey, $xDefault);
	}
	
	/**
	 * Get a parameter value.
	 * @param string $sKey
	 * @param mixed  $xDefault
	 * @return mixed
	 */
	protected function getParam($sKey, $xDefault = NULL) {
		return $this->app->getParam($sKey, $xDefault);
	}
	
	
	/**
	 * Prevent duplications.
	 */
	public function __clone() {
	}
	
	
	/**
	 * Print redirect meta html tag.
	 * @param string $url Url to redirect. Without it, reloads the page.
	 */
	protected function redirect($url = null) {
		if ($url) {
			die("<meta http-equiv='Refresh' content='0;url={$url}'>");
		} else {
			die("<meta http-equiv='Refresh' content='0'>");
		}
	}
	
}