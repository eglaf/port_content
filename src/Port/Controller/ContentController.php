<?php

namespace Port\Controller;

use \Egf\Util;
use \Egf\Service;

/**
 * Class ContentController
 */
class ContentController extends \Egf\Ancient\Controller {
	
	/** @var Service\Template */
	protected $template;
	
	/** @var Service\MyDb\MyDb|Service\MyDb\Connection */
	protected $myDb;
	
	
	/**
	 * Initialize.
	 */
	public function init() {
		$this->template = $this->getService('template');
		$this->myDb     = $this->getService('myDb');
	}
	
	
	/**
	 * Show programs for the selected channel and date.
	 * @param string $selectedOuterChannelId
	 * @param string $selectedDate
	 */
	public function showAction($selectedOuterChannelId = '', $selectedDate = '') {
		// Redirect to itself if route is not full.
		if ( ! $selectedOuterChannelId) {
			$this->redirectToFirstChannel();
		}
		if ( ! $selectedDate) {
			$this->redirectToFirstDate($selectedOuterChannelId);
		}
		
		echo $this->template->render('Port:Content/show', [
			'selectedOuterChannelId' => $selectedOuterChannelId,
			'channels'               => $this->getAvailableChannels(),
			'dates'                  => $this->getAvailableDates($selectedOuterChannelId),
			'programs'               => $this->getPrograms($selectedOuterChannelId, $selectedDate),
		]);
	}
	
	/**
	 * Redirect the browser to the first available channel.
	 */
	protected function redirectToFirstChannel() {
		$firstChannelAndProgram = $this->myDb->query('
				SELECT ch.outer_id, pr.start_datetime
				FROM channel AS ch
				JOIN program AS pr ON pr.channel_id = ch.id
				WHERE pr.start_datetime > NOW()
				ORDER BY pr.start_datetime ASC
				LIMIT 0, 1;
		')->fetch_assoc();
		if ( ! $firstChannelAndProgram) {
			throw new \Exception('Cannot load first channel from database... maybe there are no channels imported yet.');
		}
		
		$date = (new \DateTime($firstChannelAndProgram['start_datetime']))->format('Y-m-d');
		$this->redirect("/channel-content/{$firstChannelAndProgram['outer_id']}/{$date}");
	}
	
	/**
	 * Redirect to the browser to the first available date for the channel.
	 * @param string $selectedOuterChannelId
	 */
	protected function redirectToFirstDate($selectedOuterChannelId) {
		$firstProgram = $this->myDb->query('
				SELECT pr.start_datetime
				FROM program AS pr
				JOIN channel AS ch ON ch.id = pr.channel_id AND ch.outer_id = ?
				WHERE pr.start_datetime > NOW()
				ORDER BY pr.start_datetime ASC
				LIMIT 0, 1;
		', [$selectedOuterChannelId])->fetch_assoc();
		
		$date = (new \DateTime($firstProgram['start_datetime']))->format('Y-m-d');
		$this->redirect("/channel-content/{$selectedOuterChannelId}/{$date}");
	}
	
	/**
	 * Get available channels.
	 * @return array
	 */
	protected function getAvailableChannels() {
		return $this->myDb->query('
			SELECT ch.name, ch.outer_id
			FROM channel AS ch
			JOIN program AS pr ON pr.channel_id = ch.id AND pr.id = (SELECT pr2.id FROM program AS pr2 WHERE pr2.channel_id = ch.id AND pr2.start_datetime > NOW() LIMIT 0, 1)
			WHERE pr.start_datetime > NOW()
		')->fetch_all(MYSQLI_ASSOC);
	}
	
	/**
	 * Get available dates for the selected channel.
	 * @param string $selectedOuterChannelId
	 * @return array
	 */
	protected function getAvailableDates($selectedOuterChannelId) {
		$results  = [];
		$dateRows = $this->myDb->query('
			SELECT start_datetime
			FROM program AS pr
			JOIN channel AS ch ON ch.id = pr.channel_id AND ch.outer_id = ?
			WHERE start_datetime > NOW()
			ORDER BY start_datetime
		', [$selectedOuterChannelId])->fetch_all(MYSQLI_ASSOC);
		foreach ($dateRows as $dateRow) {
			$dateTimeFormatted = (new \DateTime($dateRow['start_datetime']))->format('Y-m-d');
			if ( ! isset($results[$dateTimeFormatted])) {
				$results[$dateTimeFormatted] = $dateTimeFormatted;
			}
		}
		
		return $results;
	}
	
	/**
	 * Get programs for channel on date.
	 * @param string $selectedOuterChannelId
	 * @param string $selectedDate
	 * @return array
	 */
	protected function getPrograms($selectedOuterChannelId, $selectedDate) {
		$endDate = (new \DateTime($selectedDate))->modify('+1 day')->format('Y-m-d 04:00:00');
		
		return $this->myDb->query('
			SELECT pr.title, pr.description, pr.start_datetime, pr.end_datetime, pr.age_limit
			FROM program AS pr
			JOIN channel AS ch ON ch.id = pr.channel_id AND ch.outer_id = ?
			WHERE pr.start_datetime > ?
			AND pr.start_datetime < ?
			ORDER BY pr.start_datetime ASC
		', [$selectedOuterChannelId, $selectedDate, $endDate])->fetch_all(MYSQLI_ASSOC);
	}
	
}
