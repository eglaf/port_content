<?php

namespace Port\Controller;

use \Egf\Util;
use \Egf\Service;

/**
 * Class LoadController
 *
 * todo FlashMessages
 */
class LoadController extends \Egf\Ancient\Controller {
	
	/** @var Service\Log */
	protected $log;
	
	/** @var Service\Template */
	protected $template;
	
	/** @var Service\Request */
	protected $request;
	
	/** @var Service\MyDb\MyDb|Service\MyDb\Connection */
	protected $myDb;
	
	/** @var \Port\Service\CurlLoader */
	protected $curl;
	
	
	/**
	 * Initialize.
	 */
	public function init() {
		$this->log      = $this->getService('log');
		$this->template = $this->getService('template');
		$this->request  = $this->getService('request');
		$this->myDb     = $this->getService('myDb');
		$this->curl     = $this->getService('curlLoader');
	}
	
	
	/**
	 * Select channels and date.
	 */
	public function selectChannelsAction() {
		$curlResult = json_decode($this->curl->loadByUrl($this->getParam('urlForChannels')));
		if ( ! isset($curlResult->channels)) {
			throw $this->log->exception("Cannot load channels from: {$this->getParam('urlForChannels')}");
		}
		
		echo $this->template->render('Port:Load/selectChannels', [
			'channels' => $curlResult->channels,
			'days'     => $this->getDays(),
		]);
	}
	
	/**
	 * Get date objects for selection.
	 * @return array
	 */
	protected function getDays() {
		$result = [];
		for ($i = 0; $i <= 7; $i ++) {
			$date = new \DateTime();
			$date->modify("+{$i} day");
			$result[] = $date;
		}
		
		return $result;
	}
	
	
	/**
	 * Load selected channels for date.
	 */
	public function loadChannelsAction() {
		$curlResult = json_decode($this->curl->loadByUrl($this->getUrlForContents()));
		if ( ! isset($curlResult->channels)) {
			throw $this->log->exception("Cannot load contents from: {$this->getParam('urlForContents')}");
		}
		// Iterate channels.
		foreach ($curlResult->channels as $outerChannel) {
			$innerChannelId = $this->getInnerChannelId($outerChannel);
			// Iterate programs.
			foreach ($outerChannel->programs as $outerProgram) {
				$this->processInnerProgram($innerChannelId, $outerProgram);
			}
		}
		
		$this->redirect('/load/select-channels/');
	}
	
	/**
	 * Get the url for contents.
	 * @return string
	 */
	protected function getUrlForContents() {
		// Check date.
		if ( ! Util::isDateTimeStringValid($this->request->getRequest('day'))) {
			$this->log->warning("Invalid dateTime string: {$this->request->getRequest('day')}");
			$this->redirect('/load/select-channels');
		}
		// Check channels.
		if ( ! $this->request->getRequest('channels')) {
			$this->redirect('/load/select-channels');
		}
		// Create url.
		$urlDate    = "?date={$this->request->getRequest('day')}";
		$urlChannel = '';
		foreach ($this->request->getRequest('channels') as $channel => $inputValue) {
			$urlChannel .= "&channel_id[]={$channel}";
		}
		
		return "{$this->getParam('urlForContents')}{$urlDate}{$urlChannel}";
	}
	
	/**
	 * Gives back the channel id from our db. If channel doesn't exists, it will be created. If name was changed it will be updated.
	 * @param object $outerChannel Outer channel from curl.
	 * @return int Inner channel id.
	 */
	protected function getInnerChannelId($outerChannel) {
		$innerChannel = $this->myDb->query('SELECT id, name FROM channel WHERE outer_id = ?', [$outerChannel->id])->fetch_assoc();
		// Create channel if doesn't exist.
		if ( ! $innerChannel) {
			$this->myDb->query('INSERT INTO channel (outer_id, name) VALUES (?, ?)', [$outerChannel->id, $outerChannel->name]);
			
			return $this->myDb->getLastInsertId();
		}
		// Update channel if name was changed.
		elseif ($outerChannel->name !== $innerChannel['name']) {
			$this->myDb->query('UPDATE channel SET name = ? WHERE outer_id = ?', [$outerChannel->name, $outerChannel->outer_id]);
		}
		
		return $innerChannel['id'];
	}
	
	/**
	 * Create program if doesn't exist or update it if it was changed.
	 * @param int    $innerChannelId
	 * @param object $outerProgram
	 */
	protected function processInnerProgram($innerChannelId, $outerProgram) {
		$innerProgram = $this->myDb->query('
			SELECT id, outer_id, channel_id, title, description, start_datetime, end_datetime, age_limit FROM program
			WHERE channel_id = ? AND outer_id = ?;', [$innerChannelId, $outerProgram->id])->fetch_assoc();
		// Create program if doesn't exist.
		if ( ! $innerProgram) {
			$this->myDb->query('
				INSERT INTO program (channel_id, outer_id, title, description, start_datetime, end_datetime, age_limit) VALUES (?, ?, ?, ?, ?, ?, ?)', [
				$innerChannelId, $outerProgram->id, $outerProgram->title, $outerProgram->short_description, $outerProgram->start_datetime, $outerProgram->end_datetime, $outerProgram->restriction->age_limit,
			]);
		}
		// Update program if it was changed.
		elseif ($this->wasProgramChanged($innerProgram, $outerProgram)) {
			echo "Program data should be updated but i cannot test that feature...<hr />";
			var_dump($innerProgram);
			echo "<hr />";
			var_dump($outerProgram);
			die("<hr />");
			$this->myDb->query('
				UPDATE program SET
				title = ?, description = ?, start_datetime = ?, end_datetime = ?, age_limit = ?', [
				$outerProgram->title, $outerProgram->short_description, $outerProgram->start_datetime, $outerProgram->end_datetime, $outerProgram->restriction->age_limit,
			]);
		}
	}
	
	/**
	 * Check if the program was changed.
	 * @param array  $innerProgram
	 * @param object $outerProgram
	 * @return bool
	 */
	protected function wasProgramChanged($innerProgram, $outerProgram) {
		return ($innerProgram['title'] !== $outerProgram->title
		        || $innerProgram['description'] !== $outerProgram->short_description
		        || $innerProgram['age_limit'] !== $outerProgram->restriction->age_limit
		        || $this->dateFormat($innerProgram['start_datetime']) !== $this->dateFormat($outerProgram->start_datetime)
		        || $this->dateFormat($innerProgram['end_datetime']) !== $this->dateFormat($outerProgram->end_datetime));
	}
	
	/**
	 * It creates a DateTime object from string, then gives back that one formatted as a string.
	 * @param string $date Differently formatted date strings.
	 * @return string Comparable date strings.
	 * todo Check with WinterTime!
	 */
	protected function dateFormat($date) {
		return (new \DateTime($date))->format('Y-m-d H:i:s');
	}
	
}
