<?php

namespace Port\Service;

/**
 * Class CurlLoader
 */
class CurlLoader extends \Egf\Ancient\Service {
	
	/**
	 * Load data from link by curl.
	 * @param $url
	 * @return mixed
	 */
	public function loadByUrl($url) {
		$cSession = curl_init();
		curl_setopt($cSession, CURLOPT_URL, $url);
		curl_setopt($cSession, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($cSession, CURLOPT_HEADER, FALSE);
		$result = curl_exec($cSession);
		curl_close($cSession);
		
		return $result;
	}
	
}

