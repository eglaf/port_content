<html>
<head>
</head>
<body>

<form action="/load/load-channels/" method="POST">
    <input type="submit" value="Submit" />
    <hr />
    <label for="day">Select day</label>
    <select id="day" name="day">
        <?php foreach ($days as $day) { ?>
            <option value="<?php echo $day->format('Y-m-d'); ?>"><?php echo $day->format('Y-m-d'); ?></option>
        <?php } ?>
    </select>
    <br />
    <table>
        <tbody>
		<?php foreach ($channels as $channel) { ?>
            <tr>
                <td><input name="channels[<?php echo $channel->id; ?>]" type="checkbox"/></td>
                <td><?php echo $channel->name; ?></td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
    <hr />
    <input type="submit" value="Submit" />
</form>

</body>
</html>