<html>
<head>
</head>
<body>

<?php foreach ($channels as $channel) { ?>
    <a href="/channel-content/<?php echo $channel['outer_id']; ?>"><?php echo $channel['name']; ?></a>
<?php } ?>

<hr/>

<?php foreach ($dates as $date) { ?>
    <a href="/channel-content/<?php echo $selectedOuterChannelId; ?>/<?php echo $date; ?>"><?php echo $date; ?></a>
<?php } ?>

<hr/>

<table>
    <thead>
    <th>Title</th>
    <th>Description</th>
    <th>Start</th>
    <th>End</th>
    <th>Age limit</th>
    </thead>
	<?php foreach ($programs as $program) { ?>
        <tbody>
        <tr>
            <td><?php echo $program['title']; ?></td>
            <td><?php echo $program['description']; ?></td>
            <td><?php echo $program['start_datetime']; ?></td>
            <td><?php echo $program['end_datetime']; ?></td>
            <td style="text-align:right;"><?php echo ($program['age_limit'] ? $program['age_limit'] : '-'); ?></td>
        </tr>
        </tbody>
	<?php } ?>
</table>

</body>
</html>