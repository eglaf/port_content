<?php

require_once(__DIR__ . '/../vendor/autoload.php');

// New EgfApp. (The true parameter means dev environment...)
$app = (new \Egf\App(true))
	->routeControllerActions();



/**
 * todo list
 *
 * flash messages
 * views+config into resource dir
 * repository+dbWhere rework
 * routing optional parameters
 * controllerActions return response object
 * validation
 * user login
 * routes in bundles
 * translations
 * command: generate egf db schema (session data length from config)
 * tempCache (sql results in cache)
 * myDb insert/update/delete together
 * .htaccess files (gitignore... in vendor)
 * renamedServices.json ?
 * security
 * test on unix
 * and a lot of other things
 */

